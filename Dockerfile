# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libevent-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/netcoinfoundation/netcoin.git /opt/netcoin
RUN cd /opt/netcoin/src/leveldb && \
	chmod +x build_detect_platform && \
	make libleveldb.a libmemenv.a && \
	cd /opt/netcoin/src && \
	make -f makefile.unix

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
	apt-get install -y  libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r netcoin && useradd -r -m -g netcoin netcoin
RUN mkdir /data
RUN chown netcoin:netcoin /data
COPY --from=build /opt/netcoin/src/netcoind /usr/local/bin/
USER netcoin
VOLUME /data
EXPOSE 11310 11311
CMD ["/usr/local/bin/netcoind", "-datadir=/data", "-conf=/data/netcoin.conf", "-server", "-txindex", "-printtoconsole"]